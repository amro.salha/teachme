from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Curriculum, PracticeProblems
from .encoders import CurriculumEncoder, ProblemEncoder
from accounts.models import UserAccount
import json

# Create your views here.
require_http_methods(['GET', 'POST'])
def list_curriculums(request):
    if request.method == 'GET':
        curriculums = Curriculum.objects.all()
        return JsonResponse({'curriculums':curriculums}, encoder=CurriculumEncoder)
    else:
        content = json.loads(request.body)
        user_email = content.get('user', {}).get('email')
        user = UserAccount.objects.get(email=user_email)
        content.pop('user', None)
        curriculum = Curriculum.objects.create(user=user, **content)
        return JsonResponse(curriculum, encoder=CurriculumEncoder, safe=False)

@require_http_methods(['GET'])
def show_curriculum(request, id):
    curriculum = Curriculum.objects.get(id=id)
    return JsonResponse(curriculum, encoder=CurriculumEncoder, safe=False)

@require_http_methods(['GET', 'POST'])
def list_problems(request):
    if request.method == 'GET':
        problems = PracticeProblems.objects.all()
        return JsonResponse({'problems':problems}, encoder=ProblemEncoder, safe=False)
    else:
        content = json.loads(request.body)
        user_email = content.get('user', {}).get('email')
        user = UserAccount.objects.get(email=user_email)
        content.pop('user', None)
        problem = PracticeProblems.objects.create(user=user, **content)
        return JsonResponse(problem, encoder=ProblemEncoder, safe=False)

@require_http_methods(['GET'])
def show_problem(request, id):
    problem = PracticeProblems.objects.get(id=id)
    return JsonResponse(problem, encoder=ProblemEncoder, safe=False)
