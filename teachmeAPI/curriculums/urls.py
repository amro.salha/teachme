from django.urls import path
from .views import list_curriculums, show_curriculum, list_problems, show_problem

urlpatterns = [
    path('curriculums/', list_curriculums, name='list_curriculums'),
    path('curriculums/<int:id>/', show_curriculum, name='show_curriculum'),
    path('problems/', list_problems, name='list_problems'),
    path('problems/<int:id>/', show_problem, name='show_problem'),
]
