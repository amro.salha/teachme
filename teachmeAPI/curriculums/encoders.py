from .common.json import ModelEncoder
from curriculums.models import Curriculum, PracticeProblems


class CurriculumEncoder(ModelEncoder):
    model = Curriculum
    properties = [
        "curriculum",
        "name",
        "imgUrl",
        "user",
        "id",
    ]
    def get_extra_data(self, o):
        return { 'user' : o.user.email }

class ProblemEncoder(ModelEncoder):
    model = PracticeProblems
    properties = [
        "problems",
        "name",
        "imgUrl",
        "id",
    ]
    def get_extra_data(self, o):
        return { 'user' : o.user.email }
