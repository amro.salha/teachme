from django.contrib import admin
from .models import Curriculum, PracticeProblems

# Register your models here.
@admin.register(Curriculum)
class CurriculumAdmin(admin.ModelAdmin):
    pass

@admin.register(PracticeProblems)
class PracticeProblemsAdmin(admin.ModelAdmin):
    pass
